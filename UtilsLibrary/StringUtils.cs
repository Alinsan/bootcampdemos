﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilsLibrary
{
    public static class StringUtils
    {
        public static string UpperLowerCase (string phrase)
        {
            var stringHelper = phrase.ToCharArray();
            var converted = string.Empty;
            for (int i = 0; i < stringHelper.Length; i++)
            {
                converted += (i%2 == 0 ? stringHelper[i].ToString().ToUpper() : stringHelper[i].ToString().ToLower());
            }
            return converted;
        }

        public static int LetterCount (string word, char letter)
        {
            return (from p in word.ToCharArray() where p == letter select p).Count();
        }

        public static bool IsPhrase(string phrase)
        {
            return (phrase.Trim().Contains(" "));
        }

    }
}
