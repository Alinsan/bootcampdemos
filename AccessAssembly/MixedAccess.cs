﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccessAssembly
{
    public class MixedAccess
    {
        protected internal int salary; // Accessible for the current assembly and derived classes from other assemby
        protected int discount; // Accessible for base class and derived class (In this or other assembly)
    }
}
