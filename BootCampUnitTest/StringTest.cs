﻿using System;
using System.Collections.Generic;
using System.Text;
using UtilsLibrary;
using Xunit;

namespace BootCampUnitTest
{
    public class StringTest
    {
        [Fact]
        public void FormatString()
        {
            var expected = "AlEjAnDrO SaNcHeZ";
            var result = StringUtils.UpperLowerCase("Alejandro Sanchez");
            Assert.Equal(expected, result);
        }

        [Fact]
        public void CountLetters()
        {
            var expected = 3;
            var result = StringUtils.LetterCount("Hello this is a very long phrase", 'e');
            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData("This is a phrase")]
        [InlineData("My name is Alejandro")]
        [InlineData("I live in La Paz")]
        public void IsAPhrase(string words)
        {
            Assert.True(StringUtils.IsPhrase(words));
        }
    }
}
