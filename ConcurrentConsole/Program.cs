﻿using System;
using System.Collections.Concurrent;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace ConcurrentConsole
{
    class Program
    {
        static int inputs = 500;
        static int itemsInBag;

        static void Main(string[] args)
        {
            string option = "";
            while (option != "Q")
            {
                Console.WriteLine("Select an option to view Concurrent Collections in action:");
                Console.WriteLine("1: BlockingCollection");
                Console.WriteLine("2: ConcurrentBag");
                Console.WriteLine("3: DictionaryConcurrent");
                Console.WriteLine("Q: Quit");
                Console.WriteLine("---------------------------------------------------------\r\n");
                option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        BlockingExample();
                        break;
                    case "2":
                        ConcurrentBagExample();
                        break;
                    case "3":
                        ConcurrentDictionaryExample();
                        break;
                    default:
                        break;
                }
            }
        }

        #region "Blocking Collection Example"
        static void BlockingExample()
        {
            // The token source for issuing the cancelation request.
            var cts = new CancellationTokenSource();

            // A blocking collection that can hold no more than 100 items at a time.
            var numberCollection = new BlockingCollection<int>(100);

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                int width = Math.Max(Console.BufferWidth, 80);
                int height = Math.Max(Console.BufferHeight, 8000);

                // Preserve all the display output for Adds and Takes
                Console.SetBufferSize(width, height);
            }

            // The simplest UI thread ever invented.
            Task.Run(() =>
            {
                if (Console.ReadKey(true).KeyChar == 'c')
                {
                    cts.Cancel();
                }
            });

            // Start one producer and one consumer.
            Task t1 = Task.Run(() => NonBlockingConsumer(numberCollection, cts.Token, "1"));
            Task t2 = Task.Run(() => NonBlockingConsumer(numberCollection, cts.Token, "2"));
            Task t3 = Task.Run(() => NonBlockingProducer(numberCollection, cts.Token, "1"));


            // Wait for the tasks to complete execution
            Task.WaitAll(t1, t2, t3);

            cts.Dispose();
            Console.WriteLine("Press the Enter key to exit.");
            Console.ReadLine();
        }

        static void NonBlockingConsumer(BlockingCollection<int> bc, CancellationToken ct, string id)
        {
            // IsCompleted == (IsAddingCompleted && Count == 0)
            while (!bc.IsCompleted)
            {
                int nextItem = 0;
                try
                {
                    if (!bc.TryTake(out nextItem, 0, ct))
                    {
                        Console.WriteLine("Consumer Task " + id + " Take Blocked");
                    }
                    else
                    {
                        Console.WriteLine("Consumer Task " + id + " Take:{0}", nextItem);
                    }
                }

                catch (OperationCanceledException)
                {
                    Console.WriteLine("Consumer Task " + id + " Taking canceled.");
                    break;
                }

                // Slow down consumer just a little to cause
                // collection to fill up faster, and lead to "AddBlocked"
                Thread.SpinWait(100000);
            }

            Console.WriteLine("\r\n" + "Consumer Task " + id + " No more items to take.");
        }

        static void NonBlockingProducer(BlockingCollection<int> bc, CancellationToken ct, string id)
        {
            int itemToAdd = 0;
            bool success = false;

            do
            {
                // Cancellation causes OCE. We know how to handle it.
                try
                {
                    // A shorter timeout causes more failures.
                    success = bc.TryAdd(itemToAdd, 2, ct);
                }
                catch (OperationCanceledException)
                {
                    Console.WriteLine("Add loop canceled.");
                    // Let other threads know we're done in case
                    // they aren't monitoring the cancellation token.
                    bc.CompleteAdding();
                    break;
                }

                if (success)
                {
                    Console.WriteLine("Producer Task " + id + "  Add:{0}", itemToAdd);
                    itemToAdd++;
                }
                else
                {
                    Console.Write("Producer Task " + id + "  AddBlocked:{0} Count = {1}", itemToAdd.ToString(), bc.Count);
                    // Don't increment nextItem. Try again on next iteration.

                    //Do something else useful instead.
                    UpdateProgress(itemToAdd);
                }
            } while (itemToAdd < inputs);

            // No lock required here because only one producer.
            bc.CompleteAdding();
        }

        static void UpdateProgress(int i)
        {
            double percent = ((double)i / inputs) * 100;
            Console.WriteLine("Percent complete: {0}", percent);
        }
        #endregion

        #region "Concurrent Bag Example"
        static void ConcurrentBagExample()
        {
            // Add to ConcurrentBag concurrently
            ConcurrentBag<int> cb = new ConcurrentBag<int>();

            Task t1 = Task.Run(() => AddItemsToBag(cb, 1, 50));
            Task t2 = Task.Run(() => AddItemsToBag(cb, 500, 100));


            // Wait for all tasks to complete
            Task.WaitAll(t1, t2);

            itemsInBag = 0;
            Task t3 = Task.Run(() => TakeItemsFromBag(cb, "1"));
            Task t4 = Task.Run(() => TakeItemsFromBag(cb, "2"));

            // Consume the items in the bag

            Task.WaitAll(t3, t4);

            Console.WriteLine($"There were {itemsInBag} items in the bag");

            // Checks the bag for an item
            // The bag should be empty and this should not print anything
            int unexpectedItem;
            if (cb.TryPeek(out unexpectedItem))
                Console.WriteLine("Found an item in the bag when it should be empty");

            Console.WriteLine("Press the Enter key to exit.");
            Console.ReadLine();
        }

        static void AddItemsToBag(ConcurrentBag<int> currentBag, int startNumber, int numberOfItems)
        {
            for (int i = startNumber; i < startNumber + numberOfItems; i++)
            {
                currentBag.Add(i);
                Console.WriteLine("Adding item to bag: " + i);
            }
        }

        static void TakeItemsFromBag(ConcurrentBag<int> currentBag, string id)
        {
            while (!currentBag.IsEmpty)
            {
                int item;
                if (currentBag.TryTake(out item))
                {
                    Console.WriteLine("Removing item Task-" + id + " " + item);
                    itemsInBag++;
                }
            }
        }
        #endregion

        #region "Concurrent Dictionary Example"
        static void ConcurrentDictionaryExample()
        {
            var rnd = new Random();
            ConcurrentDictionary<int, int> concurrentDic = new ConcurrentDictionary<int, int>();
            Parallel.For(0, 500, i =>
            {
                Console.WriteLine("Task-" + i + " started");
                Thread.Sleep(rnd.Next(10,3000));
                concurrentDic.AddOrUpdate(1, 1, (key, oldValue) => oldValue + 1);
                Console.WriteLine("Task-" + i + " finished");
            });

            Console.WriteLine("After 500 AddOrUpdates, Dictionary value[1] = {0}, should be 500", concurrentDic[1]);

            // Should return 100, as key 2 is not yet in the dictionary
            int value = concurrentDic.GetOrAdd(2, (key) => 100);
            Console.WriteLine("After initial GetOrAdd, Dictionary value[2] = {0} (should be 100)", value);

            // Should return 100, as key 2 is already set to that value
            value = concurrentDic.GetOrAdd(2, 10000);
            Console.WriteLine("After second GetOrAdd, Dictionary value[2] = {0} (should be 100)", value);

            Console.WriteLine("Press the Enter key to exit.");
            Console.ReadLine();
        }

        #endregion
    }
}
