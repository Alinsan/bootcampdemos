﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace LINQConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            string option = "";
            while (option != "Q")
            {
                Console.WriteLine();
                Console.WriteLine("LINQ Examples");
                Console.WriteLine("=============");
                Console.WriteLine("1: Eager and Lazy Execution");
                Console.WriteLine("2: Cross Join Example");
                Console.WriteLine("Q: Quit");
                Console.WriteLine("=============================\r\n");
                option = Console.ReadLine();
                switch (option)
                {
                    case "1":
                        EagerLazyExample();
                        break;
                    case "2":
                        JoinExample();
                        break;
                    default:
                        break;
                }
            }
            
        }

        static void EagerLazyExample()
        {
            // Sequence operators form first-class queries that
            // are not executed until you enumerate over them.

            int[] numbers = { 5, 4, 1, 3, 9, 8, 6, 7, 2, 0 };

            int i = 0;
            var q = from n in numbers
                    select ++i;

            // Note, the local variable 'i' is not incremented
            // until each element is evaluated (as a side-effect):
            foreach (var v in q)
            {
                Console.WriteLine($"v = {v}, i = {i}");
            }


            i = 0;
            var q2 = (from n in numbers
                     select ++i)
                     .ToList();

            // The local variable i has already been fully
            // incremented before we iterate the results:
            foreach (var v in q2)
            {
                Console.WriteLine($"v = {v}, i = {i}");
            }

                       
            var lowNumbers = from n in numbers
                             where n <= 3
                             select n;

            Console.WriteLine("First run numbers <= 3:");
            foreach (int n in lowNumbers)
            {
                Console.WriteLine(n);
            }

            for (i = 0; i < 10; i++)
            {
                numbers[i] = -numbers[i];
            }

            // During this second run, the same query object,
            // lowNumbers, will be iterating over the new state
            // of numbers[], producing different results:
            Console.WriteLine("Second run numbers <= 3:");
            foreach (int n in lowNumbers)
            {
                Console.WriteLine(n);
            }


            //Eager vs Lazy Loading
            List<Product> listado = GetProductList();
            var prueba = from p in listado where p.Category == "Vegetables" select p;
            var prueba2 = (from p in listado where p.Category == "Vegetables" select p).ToList();

            int quantity = prueba.Count();
            quantity = prueba2.Count();
            listado.Add(new Product("Pickle", "Vegetables",10));
        }

        static void JoinExample()
        {
            List<Product> productList = GetProductList();
            var categoryList = productList.Select(p => p.Category).Distinct().ToList();

            var crossJoin = from c in categoryList
                    join p in productList on c equals p.Category
                    select (Category: c, p.Name);

            Console.WriteLine();
            Console.WriteLine("Cross Join Example");
            Console.WriteLine("==================");

            foreach (var v in crossJoin)
            {
                Console.WriteLine(v.Name + ": " + v.Category);
            }

            var groupJoin = from c in categoryList
                join p in productList on c equals p.Category into ps
                select (Category: c, Products: ps);

            Console.WriteLine();
            Console.WriteLine("Group Join Example");
            Console.WriteLine("==================");

            foreach (var v in groupJoin)
            {
                Console.WriteLine(v.Category + ":");
                foreach (var p in v.Products)
                {
                    Console.WriteLine("   " + p.Name);
                }
            }

            var crossGroupJoin = from c in categoryList
                    join p in productList on c equals p.Category into ps
                    from p in ps
                    select (Category: c, p.Name);

            Console.WriteLine();
            Console.WriteLine("Cross Group Join Example");
            Console.WriteLine("========================");

            foreach (var v in crossGroupJoin)
            {
                Console.WriteLine(v.Name + ": " + v.Category);
            }

            categoryList.Add("Condiments");
            var leftJoin = from c in categoryList
                    join p in productList on c equals p.Category into ps
                    from p in ps.DefaultIfEmpty()
                    select (Category: c, ProductName: p == null ? "(No products)" : p.Name);

            Console.WriteLine();
            Console.WriteLine("Left Join Example");
            Console.WriteLine("=================");

            foreach (var v in leftJoin)
            {
                Console.WriteLine($"{v.ProductName}: {v.Category}");
            }
        }

        static List<Product> GetProductList()
        {
            var list = new List<Product>();
            list.Add(new Product("Carrot", "Vegetables", 10));
            list.Add(new Product("Lettuce", "Vegetables", 20));
            list.Add(new Product("Eggplant", "Vegetables", 30));
            list.Add(new Product("Onion", "Vegetables", 40));
            list.Add(new Product("Ground Beef", "Meat", 50));
            list.Add(new Product("T-bone Steak", "Meat", 60));
            list.Add(new Product("Milk", "Dairy", 70));
            list.Add(new Product("Cheesse", "Dairy", 80));
            list.Add(new Product("Yogurt", "Dairy", 90));
            list.Add(new Product("Salmon", "Seafood", 100));
            list.Add(new Product("Lobster", "Seafood", 110));
            list.Add(new Product("Soda", "Beverages", 120));
            list.Add(new Product("Water", "Beverages", 130));
            return list;
        }

        public class Product
        {
            public string Name { get; set; }
            public string Category { get; set; }
            public int Quantity { get; set; }

            public Product(string name, string category, int quantity)
            {
                Name = name;
                Category = category;
                Quantity = quantity;
            }
        }
    }
}
