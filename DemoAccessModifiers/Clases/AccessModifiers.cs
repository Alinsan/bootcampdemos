﻿using AccessAssembly;
using System;
using System.Collections.Generic;
using System.Text;

namespace DemoAccessModifiers.Clases
{
    class AccessModifiers : MixedAccess
    {
        public string name; // Accesible for everyone
        private int age; // Accessible only in current class
        private protected int identification; // Accessible in current class and derived class
        internal string city; // Accessible in current assembly

        public AccessModifiers()
        {
            name = "Alejandro";
            age = 33;
            identification = 1000;
            city = "La Paz";
            salary = 1000;
            discount = 10;
        }
    }
}
