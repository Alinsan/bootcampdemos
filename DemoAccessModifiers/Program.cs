﻿using DemoAccessModifiers.Clases;
using System;

namespace DemoAccessModifiers
{
    class Program
    {
        static void Main(string[] args)
        {
            AccessModifiers mainClass = new AccessModifiers();
            Console.WriteLine("Public Access Modifier");
            mainClass.name = "Alejandro Sanchez";
            Console.WriteLine("Accessing public property name: " + mainClass.name);
            Console.WriteLine("Private Access Modifier");
            //mainClass.age ERROR
            Console.WriteLine("Error on access to age");
            Console.WriteLine("Private Protected Access Modifier");
            //mainClass.identification ERROR
            Console.WriteLine("Error on access to identification on program, but available on derived class");
            Console.WriteLine("Internal Access Modifier");
            mainClass.city = "La Paz";
            Console.WriteLine("Accessing internal property city: " + mainClass.city);
            Console.WriteLine("Protected Access Modifier");
            //mainClass.discount ERROR
            Console.WriteLine("Error on access to discount on program, but available on derived class from other assembly");
            Console.WriteLine("Protected Internal Access Modifier");
            //mainClass.salary ERROR
            Console.WriteLine("Error on access to salary on program, but available on derived class from other assembly");
        }
    }
}
