﻿using DelegatesConsole.Class;
using System;
using System.Linq;

namespace DelegatesConsole
{
    class Program
    {
        delegate void Dele(int a, int b);
        delegate void DeleDyn(dynamic a, dynamic b);
        delegate int DeleInt(int a, int b);

        static void Main(string[] args)
        {
            Numbers num = new Numbers();
            Dele d1 = Numbers.GreaterThan;
            d1 += new Dele(num.Divisor);
            Dele allDelegates = d1;

            d1 = num.Divisor;
            allDelegates += d1;
            allDelegates(2, 6);
            foreach (var item in allDelegates.GetInvocationList())
            {
                Console.WriteLine(item.Method.DeclaringType.FullName + "." + item.Method.Name);
            }
            DeleDyn d3 = num.IsSame;
            Words word = new Words();
            DeleDyn d4 = word.IsSame;
            DeleDyn allSameDelegates = d3 + d4;
            allSameDelegates(3, 4);
            DeleInt d5 = num.Divide;
            DeleInt allDelegatesInt = d5;
            d5 = num.Multiply;
            allDelegatesInt += d5;
            int a = allDelegatesInt(6, 2);
            Console.WriteLine(a);
        }
    }
}
