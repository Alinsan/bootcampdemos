﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegatesConsole.Class
{
    class Words
    {
        public void IsSame(dynamic a, dynamic b)
        {
            Console.WriteLine(a == b ? "Equals" : "Not equal");
        }
    }
}
