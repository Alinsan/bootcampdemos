﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegatesConsole.Class
{
    class Numbers
    {
        public static void GreaterThan (int a, int b)
        {
            Console.WriteLine("First number is " + (a < b? "Higher": "Lower") + " than the second number");
        }

        public void Divisor (int a, int b)
        {
            Console.WriteLine("First number is " + (b % a == 0 ? "an" : "not an") + " exact divisor of the second number");
        }

        public void IsSame (dynamic a, dynamic b)
        {
            Console.WriteLine(a == b ? "Equals": "Not equal") ;
        }

        public int Multiply (int a, int b)
        {
            return a * b;
        }

        public int Divide(int a, int b)
        {
            return a / b;
        }
    }
}
